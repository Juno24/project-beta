from django.urls import path
from sales_rest.views import api_list_salespeople, api_show_salesperson, api_list_customer, api_show_customer, api_list_sales, api_show_sale


urlpatterns = [
    path("salespeople/", api_list_salespeople, name="list_salespeople"),
    path("salespeople/<int:id>/", api_show_salesperson, name="show_salesperson"),
    path("customers/", api_list_customer, name="list_customer"),
    path("customers/<int:id>/", api_show_customer, name="show_customer"),
    path("sales/", api_list_sales, name="list_sales"),
    path("sales/<int:id>/", api_show_sale, name="show_sale"),
]
