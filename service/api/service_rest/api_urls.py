from django.urls import path
from .views import api_list_technician, api_detail_technician, api_list_appointments, api_detail_appointment_cancel, api_detail_appointment_finish, api_detail_appointment

urlpatterns = [
    path("technicians/", api_list_technician, name = "list"),
    path("technicians/<int:pk>/", api_detail_technician, name="show"),
    path("appointments/", api_list_appointments, name="list_appointment"),
    path("appointments/<int:id>/", api_detail_appointment, name="detail_appointment"),
    path("appointments/<int:id>/finish/", api_detail_appointment_finish, name="finish_appointment"),
    path("appointments/<int:id>/cancel/", api_detail_appointment_cancel, name="cancel_appointment"),
]
