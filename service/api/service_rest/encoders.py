from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder


class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVO
    properties = {
        "vin",
        "import_href",
    }

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = {
        "first_name",
        "last_name",
        "employee_id",
        "id",
    }

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = {
        "date_time",
        "reason",
        "status",
        "technician",
        "vin",
        "customer",
        "id",
    }
    encoders = {
        "technician": TechnicianEncoder()
    }
