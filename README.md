# CarCar

Team:

* Juno - Service
* Gavin - Sales

## Design
The design of our app uses a React front-end to display in the browser. This allows us to create a front-end that can independently refresh its components to display information as needed as well as allows us to change and update components when desired without breaking or stopping the entire application. We utilized bootstrap to be more efficient.

## Service microservice

Created AutomobileVO model, with the properties "vin" and "import_href", as the value object to connect service microservice with inventory. Technician has to be created to add a service(appointment), which is why it's a foreignkey for Appointment model. The integration script is the pollar.py, which is matching the vin properties for inventory's automobile model and service's AutomobileVO, which grants access to create, add, edit and remove

## Sales microservice

The sales microservice revolves tracking customers, salespeople, and of course, sales. A sale can only be made by choosing from the database of existing customers, salespeople, and automobiles. The sales microservice communicates with the inventory microservice using an AutomobileVO which polls to the inventory microservice allowing us to grab automobiles
