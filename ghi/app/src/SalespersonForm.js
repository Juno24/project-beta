import React from 'react';


class SalespersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            employee_id: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeFirst_Name = this.handleChangeFirst_Name.bind(this);
        this.handleChangeLast_Name = this.handleChangeLast_Name.bind(this);
        this.handleChangeEmployee_ID = this.handleChangeEmployee_ID.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ salesperson: data.salesperson });
        }
    }

    handleChangeFirst_Name(event) {
        const value = event.target.value;
        this.setState({ first_name: value });
    }

    handleChangeLast_Name(event) {
        const value = event.target.value;
        this.setState({ last_name: value });
    }

    handleChangeEmployee_ID(event) {
        const value = event.target.value;
        this.setState({ employee_id: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.salesperson;

        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salespeopleUrl, fetchConfig);
        if (response.ok) {
            console.log(response)
            const newSalesperson = await response.json();
            console.log(newSalesperson);
            this.setState({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Salesperson</h1>
            <form onSubmit={this.handleSubmit} id="create-slaesperson-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeFirst_Name} value={this.state.first_name} placeholder="First Name" required type="text" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeLast_Name} value={this.state.last_name} placeholder="Last Name" required type="text" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeEmployee_ID} value={this.state.employee_id} placeholder="Employee ID" type="text" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default SalespersonForm;
