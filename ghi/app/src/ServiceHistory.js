function ServiceHistory(props) {

    return (
      <div className="container">
        <h3>Search History</h3>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Vin</th>
              <th>date time</th>
              <th>Reason</th>
              <th>technician</th>
              <th>Customer</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {props.appointments.map(appointment => {
              return (
                <tr key={appointment.id}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.date_time }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.technician.first_name }</td>
                  <td>{ appointment.customer }</td>
                  <td>{ appointment.status }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
  export default ServiceHistory;
