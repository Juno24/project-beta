function AutosList(props) {
    return (
        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">Automobiles List</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
            <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {props.autos.map(autos => {
              return (
                <tr>
                  <td>{ autos.vin }</td>
                  <td>{ autos.color }</td>
                  <td>{ autos.year }</td>
                  <td>{ autos.model.name }</td>
                  <td>{ autos.model.manufacturer.name }</td>
                  <td>{ JSON.stringify(autos.sold) }</td>
                </tr>
              );
            })}
            </tbody>
          </table>
            </p>
          </div>
        </div>
      );
}
export default AutosList;
