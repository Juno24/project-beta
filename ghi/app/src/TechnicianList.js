function TechnicianList(props) {

    return (
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>first_name</th>
              <th>last_name</th>
              <th>employee_id</th>
            </tr>
          </thead>
          <tbody>
            {props.technician.map(technicians => {
              return (
                <tr>
                  <td>{ technicians.first_name }</td>
                  <td>{ technicians.last_name }</td>
                  <td>{ technicians.employee_id }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

export default TechnicianList;
