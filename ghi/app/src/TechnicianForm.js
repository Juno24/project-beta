import React from 'react';

class TechnicianForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      first_name: '',
      last_name: '',
      employee_id: '',
    };
    this.handleChangeFirst_Name= this.handleChangeFirst_Name.bind(this);
    this.handleChangeLast_Name = this.handleChangeLast_Name.bind(this);
    this.handleChangeEmployee_Id = this.handleChangeEmployee_Id.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technician: data.technician });
    }
  }

  handleChangeFirst_Name(event) {
    const value = event.target.value;
    this.setState({ first_name: value })
  }

  handleChangeLast_Name(event) {
    const value = event.target.value;
    this.setState({ last_name: value })
  }

  handleChangeEmployee_Id(event) {
    const value = event.target.value;
    this.setState({ employee_id: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.technician;

    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newTechnicians = await response.json();
      console.log(newTechnicians);

      this.setState({
        first_name: '',
        last_name: '',
        employee_id: '',
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Technician</h1>
            <form onSubmit={this.handleSubmit} id="create-technicians-form">
              <div className="form-floating mb-3">
                <input value={this.state.first_name} onChange={this.handleChangeFirst_Name} placeholder="First Name" required type="text" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.last_name} onChange={this.handleChangeLast_Name} placeholder="Last Name" required type="text" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.employee_id} onChange={this.handleChangeEmployee_Id} placeholder="Employee id" required type="text" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee id</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
