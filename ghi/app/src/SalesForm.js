import React from 'react';


class SalesForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobile: '',
            vins: [],
            salesperson: '',
            salespeople: [],
            customer: '',
            customers: [],
            price: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
        this.handleChangeSalesperson = this.handleChangeSalesperson.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this)
    }

    async componentDidMount() {
        const url1 = 'http://localhost:8100/api/automobiles/'
        const response1 = await fetch(url1);
        if (response1.ok) {
            const data1 = await response1.json();
            this.setState({ vins: data1.autos });
        }

        const url2 = 'http://localhost:8090/api/salespeople/'
        const response2 = await fetch(url2);
        if (response2.ok) {
            const data2 = await response2.json();
            this.setState({ salespeople: data2.salesperson });
        }

        const url3 = 'http://localhost:8090/api/customers/'
        const response3 = await fetch(url3);
        if (response3.ok) {
            const data3 = await response3.json();
            this.setState({ customers: data3.customer });
        }
    }


    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({ automobile: value });
    }

    handleChangeSalesperson(event) {
        const value = event.target.value;
        this.setState({ salesperson: value });
    }

    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }

    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({ price: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.vins;
        delete data.customers;
        delete data.salespeople;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        console.log(data)
        if (response.ok) {
            console.log(response)
            const newSale = await response.json();
            console.log(newSale);
            this.setState({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Sale</h1>
            <form onSubmit={this.handleSubmit} id="create-sales-form">
              <div className="form-floating mb-3">
                <select onChange={this.handleChangeAutomobile} required name="automobile" id="automobile" className="form-select">
                    <option value="">Choose a VIN number</option>
                    {this.state.vins.map(autos => {
                        return (
                            <option key={autos.id} value={autos.vin}>{autos.vin}</option>
                        )
                    })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <select onChange={this.handleChangeSalesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a Salesperson</option>
                    {this.state.salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                        )
                    })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <select onChange={this.handleChangeCustomer} required name="customer" id="customer" className="form-select">
                    <option value="">Choose a Customer</option>
                    {this.state.customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                        )
                    })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePrice} value={this.state.price} placeholder="Price" required type="number" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default SalesForm;
