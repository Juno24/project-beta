import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));


async function loadCustomers() {
  const response = await fetch('http://localhost:8090/api/customers/');
  if (response.ok) {
    const data = await response.json();
    return data.customer;
  } else {
    console.error(response);
  }
}

async function loadSales() {
  const response = await fetch('http://localhost:8090/api/sales/');
  if (response.ok) {
    const data = await response.json();
    return data.sales;
  } else {
    console.error(response);
  }
}

async function loadAutomobiles() {
  const response = await fetch('http://localhost:8100/api/automobiles/');
  if (response.ok) {
    const data = await response.json();
    return data.autos;
  } else {
    console.error(response);
  }
}

async function loadManufacturers() {
  const response = await fetch('http://localhost:8100/api/manufacturers/');
  if (response.ok) {
    const data = await response.json();
    return data.manufacturers;
  } else {
    console.error(response);
  }
}

async function loadServices() {
  const response = await fetch('http://localhost:8080/api/appointments/');
  if (response.ok) {
    const data = await response.json();
    return data.appointments;
  } else {
    console.error(response)
  }
}

async function loadTechnicians() {
  const response = await fetch('http://localhost:8080/api/technicians/');
  if (response.ok) {
    const data = await response.json();
    return data.technician;
  } else {
    console.error(response)
  }
}

async function loadSalesperson() {
  const response = await fetch('http://localhost:8090/api/salespeople/');
  if (response.ok) {
    const data = await response.json();
    return data.salesperson;
  } else {
    console.error(response);
  }
}


async function loadModels() {
  const response = await fetch('http://localhost:8100/api/models/');
  if (response.ok) {
    const data = await response.json();
    return data.models;
  } else {
    console.error(response);
  }
}


Promise.all([
  loadCustomers(),
  loadSales(),
  loadAutomobiles(),
  loadManufacturers(),
  loadSalesperson(),
  loadModels(),
  loadServices(),
  loadTechnicians()
]).then(([
  customers,
  sales,
  autos,
  manufacturers,
  salesperson,
  models,
  appointments,
  technician,

]) => {
  root.render(
    <React.StrictMode>
      <App
      customers={customers}
      sales={sales}
      autos={autos}
      manufacturers={manufacturers}
      salesperson={salesperson}
      models={models}
      appointments={appointments}
      technician={technician}
      />
    </React.StrictMode>
  );
});
