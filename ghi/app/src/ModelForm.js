import React from 'react';


class ModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            manufacturer_id: '',
            manufacturers: [],
            picture_url: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangePicture_Url = this.handleChangePicture_Url.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers });
        }
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer_id: value });
    }

    handleChangePicture_Url(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers;

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            console.log(response)
            const newModel = await response.json();
            console.log(newModel);
            this.setState({
                name: '',
                manufacturer_id: '',
                picture_url: '',
            });
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Model</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePicture_Url} value={this.state.picture_url} placeholder="Model Picture" required type="url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Model Picture</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={this.handleChangeManufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                    <option value="">Choose a Manufacturer</option>
                    {this.state.manufacturers.map(manufacturer => {
                        return (
                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                        )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default ModelForm;
