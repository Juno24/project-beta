function ServiceList(props) {

  async function handleFinish(id) {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ status: 'finished' })
      });
  }

  async function handleCancel(id) {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ status: 'cancel' })
      });
  }

  return (
    <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>date time</th>
            <th>Reason</th>
            <th>technician</th>
            <th>Customer</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {props.appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{ appointment.vin }</td>
                <td>{ appointment.date_time }</td>
                <td>{ appointment.reason }</td>
                <td>{ appointment.customer }</td>
                <td>{ appointment.technician.first_name }</td>
                <td>
                  <button onClick={async () => await handleFinish(appointment.id)} type="button">
                  Finish</button>
                  <button onClick={async () => await handleCancel(appointment.id)} type="button">
                  Cancel</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ServiceList;
