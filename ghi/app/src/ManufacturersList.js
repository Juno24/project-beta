function ManufacturersList(props) {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Manufacturers List</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {props.manufacturers.map(manufacturers => {
            return (
              <tr>
                <td>{ manufacturers.name }</td>
              </tr>
            );
          })}
          </tbody>
        </table>
          </p>
        </div>
      </div>
    );
  }

  export default ManufacturersList;
