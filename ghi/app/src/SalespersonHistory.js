import React from 'react';


class SalespersonHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salesperson: '',
            customer: '',
            vin: '',
            price: '',
            salespeople: [],
            sales: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeSalesperson = this.handleChangeSalesperson.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
    }

    async componentDidMount() {
        const url1 = 'http://localhost:8090/api/salespeople/'
        const response1 = await fetch(url1);
        if (response1.ok) {
            const data1 = await response1.json();
            this.setState({ salespeople: data1.salesperson });
        }

        const url2 = 'http://localhost:8090/api/sales/'
        const response2 = await fetch(url2)
        if (response2.ok) {
          const data2 = await response2.json();
          this.setState({ sales: data2.sales })
        }
    }

    handleChangeSalesperson(event) {
        const value = event.target.value;
        this.setState({ salesperson: value });
    }

    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }

    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleChangePrice(event) {
      const value = event.target.value;
      this.setState({ price: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            console.log(response)
            const saleHistory = await response.json();
            console.log(saleHistory);
            this.setState({
              salesperson: '',
              customer: '',
              vin: '',
              price: '',
            });
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Salesperson History</h1>
            <form onSubmit={this.handleSubmit} id="salesperson-history-form">
            <select onChange={this.handleChangeSalesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a Salesperson</option>
                    {this.state.salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                        )
                    })}
                </select>
            </form>
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.sales.map(sales => {
            return (
              <tr>
                <td>{ sales.salesperson.first_name } { sales.salesperson.last_name }</td>
                <td>{ sales.customer.first_name} { sales.customer.last_name }</td>
                <td>{ sales.automobile.vin }</td>
                <td>{ sales.price }</td>
              </tr>
            );
          })}
          </tbody>
          </table>
          </div>
        </div>
      </div>
        );
    }
}

export default SalespersonHistory;
