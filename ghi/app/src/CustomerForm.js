import React from 'react';

class CustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            address: '',
            phone_number: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeFirst_Name = this.handleChangeFirst_Name.bind(this);
        this.handleChangeLast_Name = this.handleChangeLast_Name.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleChangePhone_Number = this.handleChangePhone_Number.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ customer: data.customer });
        }
    }

    handleChangeFirst_Name(event) {
        const value = event.target.value;
        this.setState({ first_name: value });
    }

    handleChangeLast_Name(event) {
        const value = event.target.value;
        this.setState({ last_name: value });
    }

    handleChangeAddress(event) {
        const value = event.target.value;
        this.setState({ address: value });
    }

    handleChangePhone_Number(event) {
        const value = event.target.value;
        this.setState({ phone_number: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.customer;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            console.log(response)
            const newCustomer = await response.json();
            console.log(newCustomer);
            this.setState({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            });
        }
    }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Customer</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeFirst_Name} value={this.state.first_name} placeholder="First Name" required type="text" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeLast_Name} value={this.state.last_name} placeholder="Last Name" required type="text" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeAddress} value={this.state.address} placeholder="Address" type="text" id="address" className="form-control" />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePhone_Number} value={this.state.phone_number} placeholder="Phone Number" required type="text" id="phone_number" className="form-control" />
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default CustomerForm;
