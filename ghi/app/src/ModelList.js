function ModelList(props) {
    return (
        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">Model List</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
            <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Manufacture</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {props.models.map(models => {
              return (
                <tr>
                  <td>{ models.name }</td>
                  <td>{ models.manufacturer.name }</td>
                  <td><img src={ models.picture_url }></img></td>
                </tr>
              );
            })}
            </tbody>
          </table>
            </p>
          </div>
        </div>
      );
}
export default ModelList;
